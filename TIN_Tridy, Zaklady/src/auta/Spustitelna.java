package auta;

public class Spustitelna {
	
	public static void main(String[] args) {
		Auto a1 = new Auto();
		a1.setZnacka("auto1");
		a1.setCena(100);
		
		Auto a2 = new Auto();
		a2.setZnacka("auto2");
		a2.setCena(500);
		
		Auto a3 = a1;
		Auto a4 = a2;
		
		System.out.println(a1.getZnacka() + " cena: "+ a1.getCena());
		System.out.println(a2.getZnacka() + " cena: "+ a2.getCena());
		System.out.println(a3.getZnacka() + " cena: "+ a3.getCena());
		System.out.println(a4.getZnacka() + " cena: "+ a4.getCena());
	
	}

}
