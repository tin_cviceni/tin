package kruskal;

public class Hrana {
	
	private Graf g;
	private int cena;
	private Uzel odkud;
	private Uzel kam;
	
	

	public Graf getG() {
		return g;
	}



	public void setG(Graf g) {
		this.g = g;
	}



	public int getCena() {
		return cena;
	}



	public void setCena(int cena) {
		this.cena = cena;
	}



	public Uzel getOdkud() {
		return odkud;
	}



	public void setOdkud(Uzel odkud) {
		this.odkud = odkud;
	}



	public Uzel getKam() {
		return kam;
	}



	public void setKam(Uzel kam) {
		this.kam = kam;
	}



	public Hrana(Uzel u1, Uzel u2, int cena) {
		this.cena = cena;
		this.odkud = u1;
		this.kam = u2;
	}



	
	

}
