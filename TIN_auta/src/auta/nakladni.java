package auta;

public class nakladni extends automobil {

	private int VINprives;
	
	
	public nakladni(int VIN, String nazev, String barva) {
		super(VIN, nazev, barva);
		
	}


	public int getVINprives() {
		return VINprives;
	}


	public void setVINprives(int vINprives) {
		VINprives = vINprives;
	}


	@Override
	public String toString() {
		return "nakladni [VIN= "+ super.getVIN() + " nazev= "+super.getNazev()+ " barva= "+ super.getBarva() +" VIN prives=" + VINprives + "]";
	}
	
	

}
