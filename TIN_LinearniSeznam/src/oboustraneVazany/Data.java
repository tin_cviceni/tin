package oboustraneVazany;

public class Data {
	
	private int data;
	private Data next;
	private Data prev;
	
	public Data(int data) {
		this.data = data;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Data getNext() {
		return next;
	}

	public void setNext(Data next) {
		this.next = next;
	}

	public Data getPrev() {
		return prev;
	}

	public void setPrev(Data prev) {
		this.prev = prev;
	}
	
	
}
