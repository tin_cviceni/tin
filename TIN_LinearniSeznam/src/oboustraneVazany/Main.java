package oboustraneVazany;



public class Main {
	public static void main(String[] args) {
		LinearniList myList = new LinearniList();
		
		System.out.println("----Je seznam prazdny?----");
        System.out.println(myList.isEmpty());
        
        //pridani dat do seznamu
        myList.addFirst(1);
        myList.addFirst(2);
        myList.addFirst(3);
        
        myList.addFirst(10);
        System.out.println("----Naplneny seznam----");
        myList.print();
        
        System.out.println("----Obsahuje seznam cislo 10?----");
        System.out.println(myList.contains(10));
        
        //odstraneni prvniho prvku
        System.out.println("----Obsahuje seznam cislo 10?----");
        myList.removeFirstData();
        System.out.println(myList.contains(10));
        
        myList.addLast(88);
        myList.addLast(44);
       
        
        myList.print();
        
        myList.removeLastData();
        
        
        System.out.println("----List po vymazani posledniho----");
        myList.print();
	}

}
