package oboustraneVazany;

import java.util.Iterator;



public class LinearniList {
	private Data first = null;
	private Data last = first;
	
	public LinearniList () {
		
	}
	
	public void addFirst(int data) {
		
		Data d1 = new Data(data);
		if (this.isEmpty()) { //pokud je seznam prazdny
			this.last = d1;	//nastav data jako posledni
		} else {			//pokud seznam neni prazdny
			this.first.setPrev(d1);	//nastav nova data pred puvodni prvni data
		}
		
		d1.setNext(this.first); //nastav puvodni prvni data jako nasledujici novych dat
		this.first = d1;	//nastav nova data jako prvni
	}
	
	public boolean isEmpty() {
		return this.first == null;
	}
	
	public void addLast(int data) {
		Data d1 = new Data(data);
		if (this.isEmpty()) {
			this.last = d1;
		} else {
			this.last.setNext(d1); //uloz nova data za posledni data
		}
		d1.setPrev(this.last);
		this.last = d1;
	}
	
	public void removeFirstData() {
		if (!this.isEmpty()) {
			this.first = this.first.getNext();
			this.first.setPrev((Data)null);
		}
	}
	
	public void removeLastData() {
		if (!this.isEmpty()) {
			Data n = last;
			n.getPrev().setNext(null);
		}
	}
	
	public boolean contains(int data) {
		for(Data i = this.first; i != null; i = i.getNext()) {
			if (i.getData() == data) {
				return true;
			}
		}
		return false;
	}
	
	public void print() {
		 for(Data i = this.first; i != null; i = i.getNext()) {
	            System.out.println(i.getData());
	        }
	}

}
