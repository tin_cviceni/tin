package grafUzel;


public class Main {
	
	public static void main(String[] args) {
		Uzel u1 = new Uzel("uzel1");
		Uzel u2 = new Uzel("uzel2");
		Uzel u3 = new Uzel("uzel3");
		Uzel u4 = new Uzel("uzel4");
		Uzel u5 = new Uzel("uzel5");
		Uzel u6 = new Uzel("uzel6");
		
		u1.pridejSouseda(u2);
		u1.pridejSouseda(u4);
		u1.pridejSouseda(u5);
		u1.pridejSouseda(u6);
		
		u2.pridejSouseda(u1);
		u2.pridejSouseda(u3);
		u2.pridejSouseda(u5);
		
		u5.pridejSouseda(u4);
		
		u2.vypisSousedy();
		System.out.println("-------");
		
		u1.vypisSousedy();
		System.out.println("-------");
	}

}
