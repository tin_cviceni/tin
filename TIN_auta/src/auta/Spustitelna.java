package auta;

public class Spustitelna {
	
	public static void main(String[] args) {
		centralniRegistr c = new centralniRegistr();
		
		specialni s = new specialni(1234, "zetor", "modra");
		c.pridejKradeneAuto(s);
		
		osobni o = new osobni(5678, "skoda", "zluta");
		c.pridejKradeneAuto(o);
		
		nakladni n = new nakladni(1244, "iveco", "seda");
		c.pridejKradeneAuto(n);
		
		nakladni n2 = new nakladni(1111, "iveco2", "cerna");
		n2.setVINprives(12333);
		c.pridejKradeneAuto(n2);
		
		c.vypisKradeneAuto(1234);
		c.vypisKradeneAuto(5678);
		c.vypisKradeneAuto(1244);
		c.vypisKradeneAuto(1111);
		
		
	}

}
