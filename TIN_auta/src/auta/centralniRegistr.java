package auta;

import java.util.HashMap;
import java.util.Iterator;


public class centralniRegistr {
	
	HashMap<Integer , automobil> kradena = new HashMap<>();
	automobil a = null;
	
	public centralniRegistr() {}
	
	
	public void pridejKradeneAuto(automobil a) {
		kradena.put(a.getVIN(), a);
	}
	
	public void vypisKradeneAuto(int VIN) {
		automobil a = kradena.get(VIN);
		System.out.println(a);
	}
	
}
