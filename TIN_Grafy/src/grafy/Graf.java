package grafy;

import java.util.HashMap;

public class Graf {
	
	private static HashMap<String, Uzel> uzly = new HashMap<>();
	private static int cena =0;
	public Graf() {}
	
	public void pridejHranu(String uzel1, String uzel2, int cena) {
		Uzel u1 = (Uzel)uzly.get(uzel1); //kontroluje jestli je uzel v hashmape
		Uzel u2 = (Uzel)uzly.get(uzel2);
		
		if (u1 == null) {			//pokud uzel neni v hashmape
			u1 = new Uzel(uzel1);	//vytvori novy uzel
			uzly.put(uzel1, u1);	//vlozi uzel do hashmapy
			
		}
		
		if (u2 == null) {
			u2 = new Uzel(uzel2);
			uzly.put(uzel2, u2);
		}
		
		u1.pridejSouseda(u2, cena);	//prida souseda
		u2.pridejSouseda(u1, cena);
	}
	
	public void getCenaMeziUzly(String name1, String name2) {
		Uzel u1 = Graf.getUzel(name1);
		Uzel u2 = Graf.getUzel(name2);
		cena += u1.getCena(u2);
		System.out.println("Cena mezi uzly: "+ cena);
	}
	
	public static Uzel getUzel(String name) {
		return (Uzel)uzly.get(name);
	}
	
	

}
