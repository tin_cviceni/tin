package kruskal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Kruskal {
	
	private Graf g;
	
	public Kruskal(Graf g) {
		this.g = g;
	}
	
	public ArrayList<Hrana> getSpanningTree(){
		//vytvor les
		HashMap<Uzel, Set<Uzel>> les = new HashMap<Uzel, Set<Uzel>>();
		inicializujLes(les);
		ArrayList<Hrana> minSpanTree = new ArrayList<Hrana>();
		nalezeniKostryGrafu(les, minSpanTree);
		
		return minSpanTree;
		
	}

	private void nalezeniKostryGrafu(HashMap<Uzel, Set<Uzel>> les, ArrayList<Hrana> minSpanTree) {
		LinkedList<Hrana> hrany = new LinkedList<>();
		hrany.addAll(g.getHrany());
		
		Collections.sort(hrany, new ComparatorHranDleCeny()); //seradi hrany podle ceny
		
		for(Hrana hrana: hrany) {
			Set<Uzel> mnozinaUzlu1 = les.get(hrana.getOdkud());
			Set<Uzel> mnozinaUzlu2 = les.get(hrana.getKam());
			if (!mnozinaUzlu1.equals(mnozinaUzlu2)) {
				minSpanTree.add(hrana); //pridej hranu do minSpanTree
				
				mnozinaUzlu1.addAll(mnozinaUzlu2);
				
				//pro kazdy uzel aktualizuj mapovani Uzel --> mnotina na novou mnozinu
				for(Uzel u: mnozinaUzlu1) {
					les.put(u, mnozinaUzlu1);
				}
			}
		}
		
		System.out.println("Minimalni kostra grafu obsahuje tyto hrany: ");
		for (Hrana h: minSpanTree) {
			System.out.println(h.getOdkud().getValue() + " - " + h.getKam().getValue() + " : " + h.getCena());
		}
		
	}

	private void inicializujLes(HashMap<Uzel, Set<Uzel>> les) {
		for(Uzel vrchol: g.getVrcholy()) {
			Set<Uzel> vs = new HashSet<Uzel>();
			vs.add(vrchol);
			les.put(vrchol, vs);
		}
		
	}

}
