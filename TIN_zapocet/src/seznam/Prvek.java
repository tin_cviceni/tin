package seznam;

public class Prvek {
	
	private String name;
	
	private Prvek potomek;
	private Prvek left;
	
	
	public Prvek(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	public Prvek getPotomek() {
		return potomek;
	}

	public void setPotomek(Prvek potomek) {
		this.potomek = potomek;
	}

	public Prvek getLeft() {
		return left;
	}

	public void setLeft(Prvek left) {
		this.left = left;
	}

	@Override
	public String toString() {
		return "Prvek [name=" + name + ", right=" + potomek + ", left=" + left + "] xxxx ";
	}

	

	
	
	
	

}
