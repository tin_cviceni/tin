package grafy;



public class Main {
	public static void main(String[] args) {
	Graf g = new Graf();
		
		g.pridejHranu("1", "7", 16);
		g.pridejHranu("1", "6", 6);
		g.pridejHranu("6", "5", 5);
		g.pridejHranu("6", "7", 8);
		g.pridejHranu("5", "4", 11);
		g.pridejHranu("4", "3", 9);
		g.pridejHranu("5", "7", 10);
		g.pridejHranu("7", "8", 21);
		g.pridejHranu("4", "8", 7);
		g.pridejHranu("5", "8", 14);
		g.pridejHranu("5", "3", 18);
		g.pridejHranu("6", "3", 23);
		g.pridejHranu("2", "3", 24);
		g.pridejHranu("1", "2", 4);
		
		Uzel u1 = g.getUzel("1");
		Uzel u2 = g.getUzel("2");
		Uzel u3 = g.getUzel("3");
		Uzel u4 = g.getUzel("4");
		Uzel u5 = g.getUzel("5");
		Uzel u6 = g.getUzel("6");
		Uzel u7 = g.getUzel("7");
		Uzel u8 = g.getUzel("8");
		
		System.out.println("Prima cena mezi uzly 1 a 7: "+ u1.getCena(u7));
		
		Cesta c = new Cesta();
		c.pridejVrchol("1", "6");
		c.pridejVrchol("6", "5");
		c.pridejVrchol("5", "4");
		c.pridejVrchol("4", "3");
		
		int cena = c.getCena();
		System.out.println("Cena cesty: "+cena);
		
		g.getCenaMeziUzly("1", "6");
	}

}
