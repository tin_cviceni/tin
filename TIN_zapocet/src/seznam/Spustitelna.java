package seznam;

public class Spustitelna {
	public static void main(String[] args) {
		MujSeznam s = new MujSeznam("A");
		
		s.pridej("B");
		s.pridej("C");
		s.pridej("D");
		s.pridej("E");
		s.pridej("F");
		
		//s.najdiSouseda("A");
		s.najdiSouseda("B");
		s.najdiSouseda("C");
		s.najdiSouseda("D");
		s.najdiSouseda("E");
		
		s.vycentruj();
		
		
	}
}
