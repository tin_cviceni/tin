package countWords;

import java.util.HashMap;

public class Main {
	
	public static void main(String[] args) {
		
		HashMap<String, Integer> myMap = new HashMap<>();
		String input = "ahoj ahoj svete";
		
		input = input.toLowerCase();
		System.out.println(input);
		
		String [] slova = input.split(" "); //podle ceho se rozdeluji slova
		int pocet = slova.length;			//pocet slov v inputu
		
		for (int i =0; i <pocet; ++i) {
			String s = slova[i];
			
			if (myMap.containsKey(s)) { //pokud mapa již obsahuje slovo, zmeni se pocet o +1
				myMap.replace(s, (Integer)myMap.get(s)+1);
				
			} else {
				myMap.put(s, 1); //pokud mapa neobsahuje slovo, prida se novy zaznam
			}
		}
		
		System.out.println(myMap);
	}

}
