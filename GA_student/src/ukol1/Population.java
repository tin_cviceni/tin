
package ukol1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;

public class Population {

	private ArrayList<Chromozome> chromozomy = new ArrayList<Chromozome>();
	private int startingPopulation;

	public Population(int size) {
		this.startingPopulation = size;
		for (int i = 0; i < size; i++) {
			Chromozome ch = new Chromozome();
			ch.mutateAll();
			chromozomy.add(ch);
		}
	}

	
	public void popKrizeni(int numberOfCross) {
		for (int i = 0; i < numberOfCross; i++) {
			Chromozome ch1 = chromozomy.get(new Random().nextInt(chromozomy.size()));
			Chromozome ch1Clone = ch1.cloneChromozome();
			Chromozome ch2 = chromozomy.get(new Random().nextInt(chromozomy.size()));
			Chromozome ch2Clone = ch2.cloneChromozome();

			ch1Clone.crossOver(ch2Clone);
			this.add(ch1Clone);
			this.add(ch2Clone);
		}
	}

	
	public void mutace(int pocet, double pst) {
		for (int i = 0; i < pocet; i++) {
			Chromozome ch1 = chromozomy.get(new Random().nextInt(chromozomy.size()));
			Chromozome ch1Clone = ch1.cloneChromozome();
			ch1Clone.mutate(pst);
			this.add(ch1Clone);
		}
	}

	
	public void mutateChromozomesAll(int numberOfMutate) {
		for (int i = 0; i < numberOfMutate; i++) {
			Chromozome ch1 = chromozomy.get(new Random().nextInt(chromozomy.size()));
			Chromozome ch1Clone = ch1.cloneChromozome();
			ch1Clone.mutateAll();
			this.add(ch1Clone);
		}
	}

	
		
	public void add(Chromozome ch) {
		chromozomy.add(ch);
	}

	
	public void remove(Chromozome ch) {
		chromozomy.remove(ch);
	}

	
	
	
	public int size() {
		return chromozomy.size();
	}

	public Chromozome getNejlepsiChromozom() {
		Collections.sort(chromozomy, new ChromoComparator());
		return chromozomy.get(0);
	}

	public Population newPopTurnament() throws IOException {
		Fitness eval = new Fitness("image/orloj.jpg");
		Random rnd = new Random();
		Population p = new Population(0);
		
		p.setStartingPopulation(startingPopulation);
		
		for (int i = 0; i < startingPopulation; i++) {
			int nahodnyIndex1 = rnd.nextInt(chromozomy.size());
			Chromozome ch1 = chromozomy.get(nahodnyIndex1);
			chromozomy.remove(ch1);

			int nahodnyIndex2 = rnd.nextInt(chromozomy.size());
			Chromozome ch2 = chromozomy.get(nahodnyIndex2);
			chromozomy.remove(ch2);

			if (eval.getFitness(ch1) < eval.getFitness(ch2)) {
				p.add(ch1);
			} else {
				p.add(ch2);
			}
		}
		return p;
	}

	public int getStartingPopulation() {
		return startingPopulation;
	}

	public void setStartingPopulation(int startingPopulation) {
		this.startingPopulation = startingPopulation;
	}
	
}