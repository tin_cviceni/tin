package pruchodBinarnimStromem;

import binarniStrom.Prvek;

public class BinarniStrom {
	
	public BinarniStrom() {
		super();
	}
	
	private Prvek top = null;
	private Prvek act = null;
	
	public Prvek getTop() {
		return top;
	}
	public void setTop(Prvek top) {
		this.top = top;
	}
	public Prvek getAct() {
		return act;
	}
	public void setAct(Prvek act) {
		this.act = act;
	}
	
	public void addPrvek(int number) {
		Prvek p1 = top;
		Prvek p2 = top;
		
		if (p1 == null) {
			Prvek o = new Prvek(number);
			top = o;
		} else {
			
			while (true) {
				if (number < p1.getNumber()) { //pokud je cislo mensi nez cislo v topu
					p2 = p1;					
					p1 = p1.getLeft();			//bez doleva od topu
					if (p1==null) {				//pokud je p1 nula
						Prvek o = new Prvek(number);
						o.setParent(p2);		//nastav p2 jako rodice
						p2.setLeft(o);			//nastav o jako prvek vlevo
						break;
					}
				} else if (number > p1.getNumber()) { //pokud je cislo vetsi nez top
					p2 = p1;		
					p1 = p1.getRight();			// bez doprava od topu
					if (p1 == null) {
						Prvek o = new Prvek(number);
						o.setParent(p2);		//nastav p2 jako rodice
						p2.setRight(o);			// nastav o jako prvek vpravo
						break;
					}
				} else {
					System.out.println(number + " je již ve stromě.");
					break;
				}
			}
			
		}
	}
	
	public boolean contains(int number) {
		Prvek p = top;
		
		if (p == null) {
			System.out.println("strom je prazdny.");
			return false;
		} else {
			
			while (p != null) {
			
				if (p.getNumber() == number) {
					System.out.println(number + " je ve strome.");
					return true;
					
				} else {
					if (number < p.getNumber()) {
						p = p.getLeft();
					}
					else if (number > p.getNumber()) {
						p = p.getRight();
					}
				}
			}
			System.out.println(number + " neni ve strome.");
			return false;
		}
	}
	
	public void printTreeLeaf() {this.printSubTreeLeaf(this.top);}
	public void printSubTreeLeaf(Prvek p) {
		if (p != null) {
			if (p.getLeft() == null && p.getRight() == null) { //pokud vpravo a vlevo od p neni zadny prvek, p je top --> vytiskni p
				System.out.println(p.getNumber() + " ");
			} else {
				this.printSubTreeLeaf(p.getLeft()); //vypise vsechny vetve vlevo
				this.printSubTreeLeaf(p.getRight());// vypise vsechny vetve vpravo
			}
		}
	}
	
	
	public void preOrder() {
		this.preOrder(this.top);
		System.out.println();
	}
	
	private void preOrder(Prvek p) {
		if (p != null) {
			System.out.print(p.getNumber() + " ");
			this.preOrder(p.getLeft());
			this.preOrder(p.getRight());
		}
	}
	
	public void inOrder() {
		this.inOrder(this.top);
		System.out.println();
	}
	
	private void inOrder(Prvek p) {
		if (p != null) {
			this.inOrder(p.getLeft());
			System.out.print(p.getNumber() + " " );
			this.inOrder(p.getRight());
		}
	}
	
	public void postOrder() {
        this.postOrder(this.top);
        System.out.println();
    }

    private void postOrder(Prvek p) {
        if (p != null) {
            this.postOrder(p.getLeft());
            this.postOrder(p.getRight());
            System.out.print(p.getNumber() + " ");
        }
    }
    
    //reverse order
    
    public void reverseInOrder() {
        this.reverseInOrder(this.top);
        System.out.println();
    }

    private void reverseInOrder(Prvek u) {
        if (u != null) {
            this.reverseInOrder(u.getRight());
            System.out.print(u.getNumber() + " ");
            this.reverseInOrder(u.getLeft());
        }
    }
    
    public void reversePreOrder() {
        this.reversePreOrder(this.top);
        System.out.println();
    }

    private void reversePreOrder(Prvek p) {
        if (p != null) {
            
            this.reversePreOrder(p.getRight());
            this.reversePreOrder(p.getLeft());
            System.out.print(p.getNumber() + " ");
        }
    }
    
    public void reversePostOrder() {
        this.reversePostOrder(this.top);
        System.out.println();
    }

    private void reversePostOrder(Prvek p) {
        if (p != null) {
        	System.out.print(p.getNumber() + " ");
            this.reversePostOrder(p.getRight());
            this.reversePostOrder(p.getLeft());
            
        }
    }
	

}
