package hraCisla2;

import java.util.HashSet;
import java.util.TreeSet;


public class Main {
	public static void main(String[] args) {
		//inicializace herni desky
		Board b1 = new Board();
		System.out.println(b1);
		b1.pohyb(2); //pohyb dolu
		System.out.println(b1);
		
		Board b2 = new Board();
		System.out.println(b2);
		b2.pohyb(4); //pohyb doprava
		System.out.println(b2);
		
		System.out.println("----TreeSet----");
		
		TreeSet<Board> mnozina = new TreeSet<>();
		mnozina.add(b1);
		mnozina.add(b2);
		
		System.out.println("deska b1 = deska b2: " + b1.equals(b2));
		System.out.println("deska b2 = deska b1: "+b2.equals(b1));
		System.out.println(b1.compareTo(b2));
		System.out.println(b2.compareTo(b1));
		
		System.out.println("množstvi objektů v TreeSetu: "+mnozina.size());
		
		System.out.println("----HashSet----");
		
		HashSet<Board> mnozina2 = new HashSet<>();
		mnozina2.add(b1);
		mnozina2.add(b2);
		
		System.out.println(b1.hashCode());
		System.out.println(b2.hashCode());
		System.out.println(b1.equals(b2));
		System.out.println(b2.equals(b1));
		
		System.out.println(mnozina2.size());
	}
}
