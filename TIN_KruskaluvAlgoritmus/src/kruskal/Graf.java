package kruskal;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Graf {
	
	//mapa uzlů a jejich hodnot v grafu
	private HashMap<Integer, Uzel> vrcholy = new HashMap<>();

	//set hran
	private HashSet<Hrana> hrany = new HashSet<>();
	
	public void pridej(int idUzlu1, int idUzlu2, int cena) {
		Uzel u1 = vezmiExistujiciNeboVytvor(idUzlu1); //vytvori novy nebo vezme jiz existujici uzel
		Uzel u2 = vezmiExistujiciNeboVytvor(idUzlu2);
		
		Hrana h1 = new Hrana(u1, u2, cena); //vytvori mezi uzly hranu s cenou
		hrany.add(h1); //prida hranu do setu
	}

	private Uzel vezmiExistujiciNeboVytvor(int idUzlu1) {
		Uzel u = vrcholy.get(idUzlu1); //vyhleda v mape uzel podle id
		if (u == null) { //pokud v mape neni uzel s danym id
			Uzel novy = new Uzel(idUzlu1);	//vytvori se novy uzel
			vrcholy.put(idUzlu1, novy);		//nove vytvoreny uzel se vlozi do mapy vrcholu
			u = novy;
		}
		return u;
	}
	
	public Uzel getVrchol(int idUzlu) {
		return vrcholy.get(idUzlu);
	}
	
	public Collection<Uzel> getVrcholy(){
		return vrcholy.values();
	}
	
	public Set<Hrana> getHrany(){
		return hrany;
	}

}
