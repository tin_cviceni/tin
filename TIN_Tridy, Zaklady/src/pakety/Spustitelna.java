package pakety;

public class Spustitelna {
	
	public static void main(String[] args) {
		
		paket p1 = new paket();
		p1.setJmeno("paket 1");
		
		paket p2 = new paket();
		p2.setJmeno("paket 2");
		
		paket p3 = new paket();
		p3.setJmeno("paket 3");
		
		paket p4 = new paket();
		p4.setJmeno("paket 4");
		
		p1.setNext(p2);
		p2.setNext(p3);
		p3.setNext(p4);
		
		
		p4.setNext(p1);
		
		System.out.println(p4.getNext().getJmeno());
		
	}

}
