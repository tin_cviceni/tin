package jednosmerneVazany;


public class Main {
 public static void main(String[] args) {
	 
	 LinearniList myList = new LinearniList();
		
	 System.out.println("----Je seznam prazdny?----");
     System.out.println(myList.isEmpty());
     
     //pridani dat do seznamu
     myList.addData(1);
     myList.addData(2);
     myList.addData(3);
     
     myList.addData(10);
     System.out.println("----Naplneny seznam----");
     myList.print();
     
     System.out.println("----Obsahuje seznam cislo 10?----");
     System.out.println(myList.contains(10));
     
     //odstraneni prvniho prvku
     System.out.println("----Obsahuje seznam cislo 10?----");
     myList.removeFirstElement();
     System.out.println(myList.contains(10));
     
     
     myList.print();
}
}
