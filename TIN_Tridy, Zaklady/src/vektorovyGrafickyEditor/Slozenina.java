package vektorovyGrafickyEditor;
import java.util.Iterator;
import java.util.LinkedList;

public class Slozenina extends GrObjekt{
	
	public Slozenina(int x, int y) {
		super(x, y);
	}

	private LinkedList<GrObjekt> objekty = new LinkedList<>();

	public void pridej(GrObjekt obj) {
		this.objekty.add(obj);
	}
	
	
	@Override
	public void vykresli(int x, int y) {
		System.out.println("Slozenina: " + x + ", "+y);
		Iterator var2 = this.objekty.iterator();
		
		while(var2.hasNext()) {
			GrObjekt grObjekt = (GrObjekt)var2.next();
			grObjekt.vykresli(x, y);
		}
		
	}
	

}
