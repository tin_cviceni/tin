
package ukol1;

import java.io.IOException;

public class App {

	public static void main(String[] args) throws IOException {
		
		Population p = new Population(100);
		for (int i = 0; i < 1000; i++) {
			p.popKrizeni(50);
			p.mutace(50, 30/50);
			p = p.newPopTurnament();
			if (i % 10 == 0) {
				Chromozome nejlepsi = p.getNejlepsiChromozom();
				Fitness eval = new Fitness("image/orloj.jpg");
				long error = eval.getFitness(nejlepsi);

				ShowChromozome.show(nejlepsi, "ch"+i);
				System.out.println("Nejlepsi z evoluce " + i + " f="
						+ error);
				
			}}
	}

}