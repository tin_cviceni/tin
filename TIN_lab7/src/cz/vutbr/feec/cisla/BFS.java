package cz.vutbr.feec.cisla;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeSet;



public class BFS {
	public void vypisTahy(HraciPole zadani) {
		
		// TIP: Vyzkousejte s ruznyma strukturama
		
		// TreeSet<HraciPole> closed = new TreeSet<HraciPole>();
		// LinkedList<HraciPole> closed = new LinkedList<HraciPole>();
		 LinkedList<HraciPole> open = new LinkedList();
	     HashSet<HraciPole> closed = new HashSet();
	        open.addLast(zadani);

	        while(!open.isEmpty()) {
	            HraciPole tmp = (HraciPole)open.removeFirst();
	            closed.add(tmp);
	            if (tmp.jeReseni()) {
	                System.out.println("Konec. Řešení nalezeno");
	                System.out.println(tmp);
	                return;
	            }

	            for(int i = 1; i <= 4; ++i) {
	                HraciPole nove = tmp.klonujAPohni(i);
	                if (nove != null && !closed.contains(nove)) {
	                    open.addLast(nove);
	                }
	            }
	        }

	        System.out.println("Konec. Řešení neexistuje");
		
	}
}
