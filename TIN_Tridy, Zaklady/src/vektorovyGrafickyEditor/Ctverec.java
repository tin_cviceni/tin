package vektorovyGrafickyEditor;

public class Ctverec extends GrObjekt{
	
	public Ctverec(int x, int y, int delka) {
		super(x, y);
		this.delka = delka;
	}

	private int delka;

	@Override
	public void vykresli(int x, int y) {
		System.out.println("ctverec x: " + x + " y: "+y);
		
	}

	public int getDelka() {
		return delka;
	}

	public void setDelka(int delka) {
		this.delka = delka;
	}
	
	
}
