package pruchodBinarnimStromem;



public class Main {
	public static void main(String[] args) {
		BinarniStrom bs = new BinarniStrom();
        bs.addPrvek(3);
        bs.addPrvek(5);
        bs.addPrvek(9);
        bs.addPrvek(6);
        bs.addPrvek(7);
        bs.addPrvek(9);
        
       
        System.out.println(bs.contains(14));
        System.out.println(bs.contains(4));
        System.out.println();
        
        System.out.println("Listy");
        bs.printTreeLeaf();
        System.out.println();
        System.out.println();
        
        
        
        System.out.println("Inorder");
        bs.inOrder();
        System.out.println("Reverse Inorder");
        bs.reverseInOrder();
        
        
        System.out.println("Preorder");
        bs.preOrder();
        System.out.println("Reverse Preorder");
        bs.reversePreOrder();
        
        
       
        System.out.println("Postorder");
        bs.postOrder();
        System.out.println("Reverse Postorder");
        bs.reversePostOrder();
	}

}
