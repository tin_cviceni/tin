package vektorovyGrafickyEditor;

import java.util.ArrayList;
import java.util.Iterator;

public class Platno {
	
	private int sirka;
	private int vyska;
	
	ArrayList<GrObjekt> objekty = new ArrayList<>();
	
	public Platno(int sirka, int vyska) {
		this.sirka = sirka;
		this.vyska = vyska;
	}

	public int getSirka() {
		return sirka;
	}

	public void setSirka(int sirka) {
		this.sirka = sirka;
	}

	public int getVyska() {
		return vyska;
	}

	public void setVyska(int vyska) {
		this.vyska = vyska;
	}

	public void vypis() {
		Iterator var2 = this.objekty.iterator();
		System.out.println("Souřadnice objektů na plátně: ");
		
		while (var2.hasNext()) {
			GrObjekt a = (GrObjekt)var2.next();
			System.out.println("x: "+ a.getX() + " y: "+a.getY());
		}
	}
	
	public void vytvorRastr(String nazevSouboru) {
		System.out.println("Zapisuji soubor: "+ nazevSouboru);
		Iterator var3 = this.objekty.iterator();
		
		while (var3.hasNext()) {
			GrObjekt grobjekt = (GrObjekt)var3.next();
			grobjekt.vykresli(grobjekt.getX(), grobjekt.getY());
		}
	}
	
	public void pridej(GrObjekt obj) {
		this.objekty.add(obj);
	}
	

}
