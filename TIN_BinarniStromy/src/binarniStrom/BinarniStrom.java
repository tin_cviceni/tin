package binarniStrom;



public class BinarniStrom {
	
	private Prvek top = null;
	
	public BinarniStrom() {
		super();
	}
	
	public void addPrvek(int number) {
		Prvek p1 = top;
		Prvek p2 = top;
		
		if (p1 == null) {
			Prvek o = new Prvek(number);
			top = o;
		} else {
			
			while (true) {
				if (number < p1.getNumber()) { //pokud je cislo mensi nez cislo v topu
					p2 = p1;					
					p1 = p1.getLeft();			//bez doleva od topu
					if (p1==null) {				//pokud je p1 nula
						Prvek o = new Prvek(number);
						o.setParent(p2);		//nastav p2 jako rodice
						p2.setLeft(o);			//nastav o jako prvek vlevo
						break;
					}
				} else if (number > p1.getNumber()) { //pokud je cislo vetsi nez top
					p2 = p1;		
					p1 = p1.getRight();			// bez doprava od topu
					if (p1 == null) {
						Prvek o = new Prvek(number);
						o.setParent(p2);		//nastav p2 jako rodice
						p2.setRight(o);			// nastav o jako prvek vpravo
						break;
					}
				} else {
					System.out.println(number + " je již ve stromě.");
					break;
				}
			}
			
		}
	}
	
	
public boolean findPrvek(int number) {
		
		Prvek p = top;
		
		if (p == null) {
			System.out.println("strom je prazdny.");
			return false;
		} else {
			
			while (p != null) {
			
				if (p.getNumber() == number) {
					System.out.println(number + " je ve strome.");
					return true;
					
				} else {
					if (number < p.getNumber()) {
						p = p.getLeft();
					}
					else if (number > p.getNumber()) {
						p = p.getRight();
					}
				}
			}
			System.out.println(number + " neni ve strome.");
			return false;
		}
	}

	public Prvek getTop() {
		return top;
	}

	public void setTop(Prvek top) {
		this.top = top;
	}
	
	

}
