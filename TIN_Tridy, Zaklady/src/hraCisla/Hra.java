package hraCisla;

import java.util.Iterator;


public class Hra {
	
	public static final int NAHORU = 0;
	public static final int DOLU = 1;
	public static final int DOLEVA = 2;
	public static final int DOPRAVA = 3;
	
	private int prazdneX = 0;
	private int prazdneY = 0;
	
	private int[][] pole = new int[][] {{7,2,4},{5,0,6}, {8,3,1} };
	
	public void posun(int p) {
		int x = 0, y = 0;
		for (int i = 0; i <3; i++) {
			for(int j= 0; j <3; j++) {
				if (this.pole[i][j] == 0) {
					x = i;
					y = j;
				}
			}
			
		}
		try {
			switch(p) {
			case 0:
				this.pole[x][y] = this.pole[x-1][y];
				this.pole[x-1][y] = 0;
				break;
			case 1:
				this.pole[x][y] = this.pole[x+1][y];
				this.pole[x+1][y] = 0;
				break;
			case 2:
				this.pole[x][y] = this.pole[x][y-1];
				this.pole[x][y-1] = 0;
				break;
			case 3:
				this.pole[x][y] = this.pole[x][y+1];
				this.pole[x][y+1] = 0;
				break;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		
	}
	
	public static void main(String[] args) {
		Hra h = new Hra();
		System.out.println(h.toString());
		h.posun(h.NAHORU);
		System.out.println(h.toString());
		
	}
	
	public String toString() {
		String out = "";
		 for (int i = 0; i < this.pole.length; i++) {
	            for (int j = 0; j < this.pole[i].length; j++) {
	                out += this.pole[i][j] + " ";
	            }
	            out += "\n";
		 }
		
		return "HraciPole"
				+ "\n" + out + "";
	}

}
