package pruchodBinarnimStromem;


public class Prvek {
	private int number;
	private Prvek left = null;
	private Prvek right = null;
	private Prvek parent = null;
	
	
	
	public Prvek(int number) {
		super();
		this.number = number;
	}
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Prvek getLeft() {
		return left;
	}
	public void setLeft(Prvek left) {
		this.left = left;
	}
	public Prvek getRight() {
		return right;
	}
	public void setRight(Prvek right) {
		this.right = right;
	}
	public Prvek getParent() {
		return parent;
	}
	public void setParent(Prvek parent) {
		this.parent = parent;
	}
}
