package cz.vutbr.feec.cisla;

import java.util.Random;

public class P10BFS {
	public static void main(String[] args) {
		HraciPole h = new HraciPole();
		// TODO: vytvorit nahodne hraci pole (viz priklad P2Randomize)
		for (int i = 0; i < 10000; i++) {
			h.pohni(new Random().nextInt(4)+ 1);
		}
		System.out.println(h);
		BFS bfs = new BFS();
		bfs.vypisTahy(h);
		
	}
}
