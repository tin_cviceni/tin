package need4speedVector;


public class Main {
	
	public static void main(String[] args) {
		Mapa mojeMapa = new Mapa();
		Auto a1=new Auto();
		Auto a2=new Auto();
		Auto a3=new Auto();
		Auto a4=new Auto();
		
		a1.setNazev("BMW");
		a2.setNazev("SKODA");
		a3.setNazev("AUDI");
		a4.setNazev("VW");
		
		Mapa mapa = new Mapa();
		
		mapa.addAuto(a1);
		mapa.addAuto(a2);
		mapa.addAuto(a3);
		mapa.addAuto(a4);
		
		mapa.vypisVsechnaAuta();
		
		System.out.println(mapa.getPocetAut());
		
		System.out.println("----------");
		
		mapa.removeAuto(3);
		
		mapa.vypisVsechnaAuta();
		
		System.out.println(mapa.getPocetAut());
	}

}
