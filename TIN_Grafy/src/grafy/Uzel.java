package grafy;

import java.util.HashMap;
import java.util.HashSet;

public class Uzel {
	
	private String name;
	
	private HashSet<Uzel> sousedniUzel = new HashSet<>();
	private HashMap<Uzel, Integer> cena = new HashMap<>();
	
	public Uzel(String name) {
		this.name = name;
	}
	
	public void pridejSouseda(Uzel u, int cena) {
		this.sousedniUzel.add(u);
		this.cena.put(u, cena);
	}
	
	public int getCena(Uzel u) {return (Integer)this.cena.get(u);}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
