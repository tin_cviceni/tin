package need4speed;

public class Auto {
	private String znacka;
	private int pozice_x;
	private int pozice_y;
	
	public String getZnacka() {
		return znacka;
	}
	public void setZnacka(String znacka) {
		this.znacka = znacka;
	}
	public int getPozice_x() {
		return pozice_x;
	}
	public void setPozice_x(int pozice_x) {
		this.pozice_x = pozice_x;
	}
	public int getPozice_y() {
		return pozice_y;
	}
	public void setPozice_y(int pozice_y) {
		this.pozice_y = pozice_y;
	}
	
}
