
package ukol1;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ShowChromozome extends JPanel {
    private static final long serialVersionUID = 1L;
    private Chromozome chromozome;
    private static JFrame frame = new JFrame();

    public ShowChromozome(Chromozome ch) {
        this.chromozome = ch;
        this.setSize(256, 256);
        this.setPreferredSize(new Dimension(256, 256));
    }

    public void paintComponent(Graphics g) {
        drawGraphics(g, this.chromozome);
    }

    public static void drawGraphics(Graphics g, Chromozome chromozome) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 256, 256);
        int index = 0;

        for(int i = 0; i < 30; ++i) {
            Polygon poly = new Polygon();

            for(int j = 0; j < 5; ++j) {
                poly.addPoint(chromozome.getData(index), chromozome.getData(index + 1));
                index += 2;
            }

            Color c = new Color(chromozome.getData(index), chromozome.getData(index + 1), chromozome.getData(index + 2), chromozome.getData(index + 3));
            index += 4;
            g.setColor(c);
            g.fillPolygon(poly);
        }

    }

    public static void show(Chromozome ch, String title) {
        frame.setTitle(title);
        Container contentPane = frame.getContentPane();
        contentPane.removeAll();
        contentPane.add(new ShowChromozome(ch));
        frame.pack();
        frame.setVisible(true);
        frame.repaint();
    }
}
