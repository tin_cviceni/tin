package grafUzel;

import java.util.Vector;

public class Uzel {
	
	private String name;
	
	Vector<Uzel> soused = new Vector<>();
	
	public Uzel(String name) {
		this.name = name;
	}
	
	public void pridejSouseda(Uzel novySoused) {
		soused.add(novySoused);
	}
	
	public void vypisSousedy() {
		for(Uzel uzel: soused) {
			System.out.println(uzel.name);
		}
	}

}
