package need4speedVector;

import java.util.Vector;

public class Mapa {

	private Vector<Auto> auta = new Vector<Auto>();
	
	public void addAuto(Auto noveAuto) {
		auta.add(noveAuto);
	}
	
	public Auto getAuto(int auto) {
		return auta.get(auto);
	}
	
	public void removeAuto(int index) {
		auta.remove(index);
	}
	
	public int getPocetAut() {
		return auta.size();
	}
	
	public void vypisVsechnaAuta() {
		for (Auto auto: auta) {
			System.out.println(auto.getNazev());
		}
	}
}
