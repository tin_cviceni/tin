package jednosmerneVazany;

import java.util.Iterator;

public class LinearniList {
	
	private Data first;
	
	public LinearniList() {
		
	}
	
	public void addData(int data) { //nova data se přidají na začátek seznamu
		Data e = new Data(data);
		e.setNext(this.first);
		this.first = e;
	}
	
	public void removeFirstElement() {
		
		if (!this.isEmpty()) {
			this.first = this.first.getNext();
		}
	}
	
	public boolean isEmpty() {
		return this.first==null;
	}
	
	public boolean contains(int data) {
		for (Data i = this.first; i != null; i =i.getNext()) { //iterace celym linearnim seznamem
			if (i.getData() == data) {
				return true;
			}
		}
		return false;
	}
	
	public void print() {
		for (Data i = this.first; i != null; i = i.getNext()) {
			System.out.println(i.getData());
		}
	}

}
