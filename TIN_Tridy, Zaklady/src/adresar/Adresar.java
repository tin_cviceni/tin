package adresar;

public class Adresar {
	private String obsah;
	private Adresar left;
	private Adresar right;
	
	
	public String getObsah() {
		return obsah;
	}
	public void setObsah(String obsah) {
		this.obsah = obsah;
	}
	public Adresar getLeft() {
		return left;
	}
	public void setLeft(Adresar left) {
		this.left = left;
	}
	public Adresar getRight() {
		return right;
	}
	public void setRight(Adresar right) {
		this.right = right;
	}
	
	
	
}
