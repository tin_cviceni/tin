package auta;

import java.util.ArrayList;

public abstract class automobil {
	
		
		private int VIN;
		private String nazev;
		private String barva;
		
		public automobil(int VIN, String nazev, String barva) {
			this.VIN = VIN;
			this.nazev = nazev;
			this.barva = barva;
		}
		
		
		public int getVIN() {
			return VIN;
		}
		public void setVIN(int vIN) {
			VIN = vIN;
		}
		public String getNazev() {
			return nazev;
		}
		public void setNazev(String nazev) {
			this.nazev = nazev;
		}
		public String getBarva() {
			return barva;
		}
		public void setBarva(String barva) {
			this.barva = barva;
		}


		@Override
		public String toString() {
			return "automobil [VIN=" + VIN + ", nazev=" + nazev + ", barva=" + barva + "]";
		}
		
		
		
		
}
