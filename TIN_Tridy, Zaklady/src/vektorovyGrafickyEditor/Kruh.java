package vektorovyGrafickyEditor;

public class Kruh extends GrObjekt {
	
	public Kruh(int x, int y, int polomer) {
		super(x, y);
		this.polomer = polomer;
	}

	private int polomer;
	
	

	public int getPolomer() {
		return polomer;
	}



	public void setPolomer(int polomer) {
		this.polomer = polomer;
	}



	@Override
	public void vykresli(int x, int y) {
		System.out.println("kruh x: " + x + " y: "+y);
		
	}

}
