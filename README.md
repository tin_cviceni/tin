# TIN



## Data Types

### HashMap
- Unsynchronized and permits nulls.
- Operations: get and put
- Set<>:
    - A collection that contains no duplicate elements.
    - Sets contain no pair of elements e1 and e2 such that e1.equals(e2), and at most one null element.
- Type parametres:
       - < K > the type of keys maintained by this map
       - < V > the type of mapped values   
- hashCode()
    - Returns a hash code value for the object.
    - Returns:
        a hash code value for this object.
- equals()
    - Indicates whether some other object is "equal to" this one.
    - Parameters:
            obj the reference object with which to compare.
    - Returns:
        true if this object is the same as the obj argument; false otherwise.

### ArrayList
- Resizable-array implementation of the List interface.
- Operations: size, isEmpty, get, set, iterator, listIterator, add 

### Vector
- Growable array of object.
- Operations: remove, add, size, get, set
- Type Parameters:
       - < E > Type of component elements

### TreeSet
- Elements are ordered using their natural ordering, or by a Comparator provided at set creation time.
- Type Parameters:
       - < E > the type of elements maintained by this set

### HashSet
- Makes no guarantees as to the iteration order of the set; in particular, it does not guarantee that the order will remain constant over time. 
- Type Parameters:
       - < E > the type of elements maintained by this set

### Comparable
- Imposes a total ordering on the objects of each class that implements it. 
- This ordering is referred to as the class's natural ordering, and the class's compareTo method is referred to as its natural comparison method.
- Lists (and arrays) of objects that implement this interface can be sorted automatically by Collections.sort (and Arrays.sort). 
- Objects that implement this interface can be used as keys in a sorted map or as elements in a sorted set, without the need to specify a comparator.
- compareTo():
    - Compares this object with the specified object for order. Returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
    - Parameters:
        o the object to be compared.
    - Returns:
        a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
- Type Parameters:
      - < T > the type of objects that this object may be compared to

