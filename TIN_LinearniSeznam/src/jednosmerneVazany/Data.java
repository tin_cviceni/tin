package jednosmerneVazany;

public class Data {
	
	private int data;
	private Data next;
	
	public Data(int data) {
		this.data= data;
	}
	
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public Data getNext() {
		return next;
	}
	public void setNext(Data next) {
		this.next = next;
	}
	
	

}
