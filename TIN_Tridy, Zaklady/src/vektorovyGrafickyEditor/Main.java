package vektorovyGrafickyEditor;


public class Main {
	
	public static void main(String[] args) {
		Platno p = new Platno(600, 800);
		p.pridej(new Kruh(10, 15, 30));
		p.pridej(new Kruh(22, 4, 14));
		p.pridej(new Ctverec(50, 50, 30));
		
		Slozenina s = new Slozenina(50, 50);
		s.pridej(new Kruh(30, 30, 100));
		s.pridej(new Ctverec(100, 100, 80));
		p.pridej(s);
		
		p.vypis();
		p.vytvorRastr("mujobrazek.png");
	}

}
