package hraCisla2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class Board implements Comparable<Board> {
	
	public static final int NAHORU = 1;
	public static final int DOLU = 2;
	public static final int DOLEVA = 3;
	public static final int DOPRAVA = 4;
	
	private int[][] values = new int[][]{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
    private int pozX = 0;
    private int pozY = 0;
	
	
    public Board() {
    }
    
    public void pohyb(int smer) {
        switch (smer) {
            case 1: //SMER NAHORU
                this.values[this.pozY][this.pozX] = this.values[this.pozY - 1][this.pozX];
                this.values[this.pozY - 1][this.pozX] = 0;
                --this.pozY;
                break;
            case 2: //SMER DOLU
                this.values[this.pozY][this.pozX] = this.values[this.pozY + 1][this.pozX];
                this.values[this.pozY + 1][this.pozX] = 0;
                ++this.pozY;
                break;
            case 3: //SMER DOLEVA
                this.values[this.pozY][this.pozX] = this.values[this.pozY][this.pozX - 1];
                this.values[this.pozY][this.pozX - 1] = 0;
                --this.pozX;
                break;
            case 4: //SMER DOPRAVA
                this.values[this.pozY][this.pozX] = this.values[this.pozY][this.pozX + 1];
                this.values[this.pozY][this.pozX + 1] = 0;
                ++this.pozX;
        }

    }
    
    
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(values);
		result = prime * result + Objects.hash(pozX, pozY);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		return pozX == other.pozX && pozY == other.pozY && Arrays.deepEquals(values, other.values);
	}

	@Override
	public int compareTo(Board o) {
		 for(int y = 0; y < this.values.length; ++y) {
	            for(int x = 0; x < this.values.length; ++x) {
	                if (this.values[y][x] > o.values[y][x]) {
	                    return 1;
	                }

	                if (this.values[y][x] < o.values[y][x]) {
	                    return -1;
	                }
	            }
	        }

	        return 0;
	}
	
	public String toString() {
		String res = "";
		
		for(int y =0; y<this.values.length;++y) {
			for (int x = 0; x<this.values.length;++x) {
				res = res + this.values[y][x] + " ";
			}
			
			res = res + "\n";
		}
		return res;
	}
	

}
