package grafy;

public class Cesta {
	
	private static int cena = 0;
	
	public Cesta() {}
	
	public void pridejVrchol(String a, String b) {
		Uzel u1 = Graf.getUzel(a);
		Uzel u2 = Graf.getUzel(b);
		cena += u1.getCena(u2);
		
	}

	public static int getCena() {
		return cena;
	}

	public static void setCena(int cena) {
		Cesta.cena = cena;
	}
	
	

}
