package vektorovyGrafickyEditor;

public abstract class GrObjekt {
	private int x;
	private int y;
	
	public GrObjekt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public abstract void vykresli(int x, int y);

}
